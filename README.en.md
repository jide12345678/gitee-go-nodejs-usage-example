# Pipeline template

#### Description
{**When you're done, you can delete the content in this README and update the file with details for others getting started with your repository**}

#### Software Architecture

-Directory: `vue-demo` is a project created based on Vue scaffolding, and the build output is in the `dist` directory.
-Directory: `react-demo` is a project created based on Create React APP scaffolding, and the build output is in the `build` directory.
-Directory: `angular-demo` is a project created based on Angular CLI scaffolding, and the build output is in the `dist` directory.

#### Installation

1. Enter a subproject. Such as `vue-demo`
2. Execute the installation dependency command `npm install`
3. After the installation is complete, execute the build command `npm build`

#### Instructions

1. This warehouse contains 3 commonly used projects created by official scaffolding
2. Please refer to this warehouse to configure your actual project, such as the command and directory of the build output

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
